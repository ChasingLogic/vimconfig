red=$(tput bold; tput setaf 6)
yellow=$(tput bold; tput setaf 2)
textreset=$(tput sgr0)


echo "${red}Preparing your system Mr. Robinson.${textreset}"
echo  "${yellow}~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~${textreset}"
echo  ""
echo  "${red}Installing requisite software, Sir.${textreset}"
echo  "${yellow}~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~${textreset}"
sudo pacman -S go gcc vim scala traceroute <<EOF
y
EOF



cd ~/vimconfig/

echo  "${red}Configuring vim, just the way you like it Sir.${textreset}"
echo  "${yellow}~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~${textreset}"
/bin/bash vim.sh

mkdir ~/projects

echo  "${red}Configuring git, Sir. Because we know you like to Git-R-Done.${textreset}"
echo  "${yellow}~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~${textreset}"

git config --global user.name "Mathew Robinson"
git config --global user.email "mathew.robinson3114@gmail.com"
git config --global push.default current

echo  "${yellow}~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~${textreset}"
echo  "${red}Setting up your golang folder structure, just the way you like it Sir.${textreset}"
echo  "${yellow}~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~${textreset}"
mkdir ~/projects/go
mkdir ~/projects/go/src
mkdir ~/projects/go/pkg
mkdir ~/projects/go/bin
cd ~/vimconfig
cp bashrc ~/.bashrc

echo  "${yellow}~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~${textreset}"
echo  "${red}Removing unnecessary folders, Sir.${textreset}"
echo  "${yellow}~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~${textreset}"
cd ~/vimconfig
cd Pictures
mv * ~/Pictures

echo  "${yellow}~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~${textreset}"
echo  "${red}Have a nice day Mr. Robinson.${textreset}"
echo  "${yellow}~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~${textreset}"
rm -rf ~/vimconfig
