execute pathogen#infect()
filetype off
filetype plugin indent off
set runtimepath+=$GOROOT/misc/vim
filetype plugin indent on
syntax on
set tabstop=4
set shiftwidth=4
set expandtab
set autoindent
colorscheme evening
