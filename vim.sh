#!/bin/bash

green=$(tput bold; tput setaf 2)
blue=$(tput bold; tput setaf 6)
reset=$(tput sgr0)


gitPlugins() {
    echo "${green}~~~~~~~~~~~~~~~~~~~~"
    echo "${blue}Changing directories"
    echo "${green}~~~~~~~~~~~~~~~~~~~~"
    echo "${reset}"

    cd ~/.vim/bundle

    echo "${green}~~~~~~~~~~~~~~~~~~~~"
    echo "${blue}Grabbing NERDTree "
    echo "${green}~~~~~~~~~~~~~~~~~~~~"
    echo "${reset}"

    git clone https://github.com/scrooloose/nerdtree.git
    
    echo "${green}~~~~~~~~~~~~~~~~~~~~"
    echo "${blue}Grabbing Syntastic"
    echo "${green}~~~~~~~~~~~~~~~~~~~~"
    echo "${reset}"

    git clone https://github.com/scrooloose/syntastic.git
        
    echo "${green}~~~~~~~~~~~~~~~~~~~~"
    echo "${blue}Grabbing Solarized."
    echo "${green}~~~~~~~~~~~~~~~~~~~~"
    echo "${reset}"

    git clone https://github.com/altercation/vim-colors-solarized.git 

    echo "${green}~~~~~~~~~~~~~~~~~~~~"
    echo "${blue}Grabbing Chasing Logic and base16 Color Scheme."
    echo "${green}~~~~~~~~~~~~~~~~~~~~"
    echo "${reset}"

    git clone https://github.com/ChasingLogic/ChasingLogic-colorscheme-vim.git
    git clone https://github.com/chriskempson/base16-vim

    echo "${green}~~~~~~~~~~~~~~~~~~~~"
    echo "${blue}Grabbing golang plugins."
    echo "${green}~~~~~~~~~~~~~~~~~~~~"
    echo "${reset}"

    git clone https://github.com/fatih/vim-go.git

    echo "${green}~~~~~~~~~~~~~~~~~~~~"
    echo "${blue}Grabbing Delimit Mate."
    echo "${green}~~~~~~~~~~~~~~~~~~~~"
    echo "${reset}"

    git clone https://github.com/Raimondi/delimitMate.git 

    echo "${green}~~~~~~~~~~~~~~~~~~~~"
    echo "${blue}Grabbing Multi-Cursor."
    echo "${green}~~~~~~~~~~~~~~~~~~~~"
    echo "${reset}"

    git clone https://github.com/terryma/vim-multiple-cursors

    echo "${green}~~~~~~~~~~~~~~~~~~~~"
    echo "${blue}Grabbing Pathogen"
    echo "${green}~~~~~~~~~~~~~~~~~~~~"
    echo "${reset}"

    cd ~/
    git clone https://github.com/tpope/vim-pathogen.git
    mv ~/vim-pathogen/autoload/pathogen.vim ~/.vim/autoload
    rm -rf ~/vim-pathogen

    echo "${green}~~~~~~~~~~~~~~~~~~~~"
    echo "${blue}All done."
    echo "${blue}Goodbye."
    echo "${green}~~~~~~~~~~~~~~~~~~~~"
    echo "${reset}"
}

echo "${green}~~~~~~~~~~~~~~~~~~~~"
echo "${blue}Configuring Vim, This assumes vim and git are already installed."
echo "${green}~~~~~~~~~~~~~~~~~~~~"
echo "${reset}"
mkdir ~/.vim
mkdir ~/.vim/autoload
mkdir ~/.vim/bundle
touch ~/.vimrc

echo "${green}~~~~~~~~~~~~~~~~~~~~"
echo "${blue}Writing to .vimrc"
echo "${green}~~~~~~~~~~~~~~~~~~~~"

echo "execute pathogen#infect()" > ~/.vimrc
echo "filetype off" >> ~/.vimrc
echo "filetype plugin indent off" >> ~/.vimrc
echo "filetype plugin indent on" >> ~/.vimrc
echo "syntax on" >> ~/.vimrc
echo "set tabstop=4" >> ~/.vimrc
echo "set shiftwidth=4" >> ~/.vimrc
echo "set expandtab" >> ~/.vimrc
echo "set number" >> ~/.vimrc
echo "set history=1000" >> ~/.vimrc
echo "set undolevels=1000" >> ~/.vimrc
echo "set autoindent" >> ~/.vimrc
echo "set noswapfile" >> ~/.vimrc
echo "set nobackup" >> ~/.vimrc
echo "set title" >> ~/.vimrc
echo "let g:delimitMate_expand_cr=1" >> ~/.vimrc
echo "let g:delimitMate_expand_space=1" >> ~/.vimrc
echo "set background=dark" >> ~/.vimrc
echo "colorscheme Chasing_Logic" >> ~/.vimrc


echo "${green}~~~~~~~~~~~~~~~~~~~~"
echo "${blue}Done writing to .vimrc"
echo "${green}~~~~~~~~~~~~~~~~~~~~~~"
echo "${blue}Downloading plugins"
echo "${green}~~~~~~~~~~~~~~~~~~~~"
gitPlugins
echo "${reset}"
