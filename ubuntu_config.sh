red=$(tput bold; tput setaf 6)
yellow=$(tput bold; tput setaf 2)
textreset=$(tput sgr0)


echo "${red}Preparing your system Mr. Robinson.${textreset}"
echo  "${yellow}~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~${textreset}"
echo  ""
echo  "${red}Installing requisite software, Sir.${textreset}"
echo  "${yellow}~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~${textreset}"
sudo apt-get install golang gcc vim mongodb <<EOF
y
EOF

chmod +x node-install.sh
sudo ./node-install.sh

sudo npm install -g bower
sudo npm install -g express
sudo npm install -g express-generator
sudo npm install -g yo
sudo npm install -g generator-karma
sudo npm install -g generator-angular
sudo npm install -g gulp
sudo npm install -g grunt
sudo npm install -g grunt-cli
sudo npm install -g nodemon

cd ~/vimconfig/

echo  "${red}Configuring vim, just the way you like it Sir.${textreset}"
echo  "${yellow}~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~${textreset}"
/bin/bash vim.sh

mkdir ~/.bin
mkdir ~/projects

echo  "${red}Configuring git, Sir. Because we know you like to Git-R-Done.${textreset}"
echo  "${yellow}~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~${textreset}"

git config --global user.name "Mathew Robinson"
git config --global user.email "mathew.robinson3114@gmail.com"
git config --global push.default current

echo  "${yellow}~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~${textreset}"
echo  "${red}Setting up your golang folder structure, just the way you like it Sir.${textreset}"
echo  "${yellow}~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~${textreset}"
mkdir ~/projects/go
mkdir ~/projects/go/src
mkdir ~/projects/go/pkg
mkdir ~/projects/go/bin
cd ~/vimconfig
cp bashrc ~/.bashrc

echo  "${yellow}~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~${textreset}"
echo  "${red}Removing unnecessary folders, Sir.${textreset}"
echo  "${yellow}~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~${textreset}"
cd ~/vimconfig
cd Pictures
mv * ~/Pictures

echo  "${yellow}~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~${textreset}"
echo  "${red}Have a nice day Mr. Robinson.${textreset}"
echo  "${yellow}~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~${textreset}"
rm -rf ~/vimconfig
