cd ~/vimconfig
sudo apt-get install mercurial <<EOF
y
EOF
sudo apt-get install build-essential xorg-dev libudev-dev libts-dev libgl1-mesa-dev libglu1-mesa-dev libasound2-dev libpulse-dev libopenal-dev libogg-dev libvorbis-dev libaudiofile-dev libpng12-dev libfreetype6-dev libusb-dev libdbus-1-dev zlib1g-dev libdirectfb-dev <<EOF
y
EOF


hg clone http://hg.libsdl.org/SDL

cd SDL
./configure
make
sudo make install

sudo ldconfig
