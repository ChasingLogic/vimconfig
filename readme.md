This script is written to help myself and others use my standard vim config
with plugins.

Important notes:
    IT ASSUMES YOU HAVE VIM INSTALLED
    IT ASSUMES YOU HAVE GIT INSTALLED

If you fail to meet either requirement it will fail.

Good luck and happy vimming!
